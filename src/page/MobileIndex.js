import React from 'react';
import MobileHeader from '../components/MobileHeader';
import MobileFooter from '../components/MobileFooter';
import MobileList from '../components/MobileList';
import { Tabs, Carousel } from 'antd';
const TabPane = Tabs.TabPane;

class MobileIndex extends React.Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            autoplay: true
        }
        return (
            <div>
                <MobileHeader></MobileHeader>
                <Tabs>
                    <TabPane tab='头条' key='1'>
                        <div className='carousel'>
                            <Carousel {...settings}>
                                <div><img src='https://img.alicdn.com/tfs/TB1WexbhwoQMeJjy0FoXXcShVXa-520-280.jpg' /></div>
                                <div><img src='https://img.alicdn.com/simba/img/TB1IYw5cEQIL1JjSZFhSuuDZFXa.jpg' /></div>
                                <div><img src='https://img.alicdn.com/tfs/TB1LpZzb2BNTKJjy1zdXXaScpXa-520-280.jpg' /></div>
                                <div><img src='https://aecpm.alicdn.com/tps/i1/TB1r4h8JXXXXXXoXXXXvKyzTVXX-520-280.jpg' /></div>
                            </Carousel>
                        </div>
                        <MobileList count={20} type='top' />
                    </TabPane>
                    <TabPane tab='社会' key='2'>
                        <MobileList count={20} type='shehui' />
                    </TabPane>
                    <TabPane tab='国内' key='3'>
                        <MobileList count={20} type='guonei' />
                    </TabPane>
                    <TabPane tab='国际' key='4'>
                        <MobileList count={20} type='guoji' />
                    </TabPane>
                    <TabPane tab='娱乐' key='5'>
                        <MobileList count={20} type='yule' />
                    </TabPane>
                </Tabs>
                <MobileFooter></MobileFooter>
            </div>
        )
    }
}

export default MobileIndex;