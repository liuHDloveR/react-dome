import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import MediaQuery from 'react-responsive';

import PCIndex from './page/PCIndex'
import MobileIndex from './page/MobileIndex'
import PCNewsDetail from './components/PCNewsDetail'
import MobileNewsDetail from './components/MobileNewsDetail'
import  PCUserCenter from './components/PCUserCenter'
import MobileUserCenter from './components/MobileUserCenter'
class App extends Component {
  render() {
    return (
      <div>
        <MediaQuery query='(min-device-width: 1224px)'>
            <Router>
                <Switch>
                    <Route exact path='/' component={PCIndex} />
                    <Route path={'/details/:uniquekey'} component={PCNewsDetail} />
                    <Route path='/usercenter' component={PCUserCenter} />
                </Switch>
            </Router>
        </MediaQuery>
        <MediaQuery query='(max-device-width: 1224px)'>
            <Router>
                <Switch>
                    <Route exact path='/' component={MobileIndex} />
                    <Route path={'/details/:uniquekey'} component={MobileNewsDetail} />
                    <Route path='/usercenter' component={MobileUserCenter} />
                </Switch>
            </Router>
        </MediaQuery>
      </div>
    )
  }
}

export default App;
