import React from 'react';
// import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import PCNewsBlock from './PCNewsBlock'
import PCNewsImageBlock from './PCNewsImageBlock'
import { Row, Col, Carousel, Tabs } from 'antd';
const TabPane = Tabs.TabPane;
class PCNewsContainer extends React.Component {
    render () {
        const settings = {
            dots: true,
            infinite: true,
            speed: 500,
            slidesToShow: 1,
            autoplay: true
        }
        return (
            <div>
                <Row>
                    <Col span={2}></Col>
                    <Col span={20} className='container'>
                        <div className='leftContainer'>
                            <div className='carousel'>
                                <Carousel {...settings}>
                                    <div><img src='https://img.alicdn.com/tfs/TB1WexbhwoQMeJjy0FoXXcShVXa-520-280.jpg' /></div>
                                    <div><img src='https://img.alicdn.com/simba/img/TB1IYw5cEQIL1JjSZFhSuuDZFXa.jpg' /></div>
                                    <div><img src='https://img.alicdn.com/tfs/TB1LpZzb2BNTKJjy1zdXXaScpXa-520-280.jpg' /></div>
                                    <div><img src='https://aecpm.alicdn.com/tps/i1/TB1r4h8JXXXXXXoXXXXvKyzTVXX-520-280.jpg' /></div>
                                </Carousel>
                            </div>
                        </div>
                        <PCNewsImageBlock count={6} type='guoji' width='400px' cartTitle='国际' imageWidth='112px' />
                        <Tabs class='tabs_news'>
                            <TabPane tab='头条' key='1'>
                                <PCNewsBlock count={22} type='top' width='100%' bordered='false' />
                            </TabPane>
                            <TabPane tab='国际' key='2'>
                                    <PCNewsBlock count={22} type='guoji' width='100%' bordered='false' />
                                </TabPane>
                        </Tabs>
                        <div>
                            <PCNewsImageBlock count={8} type='guonei' width='100%' cartTitle='国内' imageWidth='132px' />
                            <PCNewsImageBlock count={16} type='yule' width='100%' cartTitle='娱乐' imageWidth='132px' />
                        </div>
                    </Col>
                    <Col span={2}></Col>
                </Row>
            </div>
        )
    }
}

export default PCNewsContainer;
