import React from 'react';
import { Row, Col, Menu, Icon, message, Form, Input, Button, Checkbox, Modal, Tabs } from 'antd';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;

class MobileHeader extends React.Component {
    constructor () {
        super()
        this.state = {
            current: 'top',
            modalVisible: false,
            action: 'login',
            hasLogined: false,
            userNickName: '',
            userid: 0
        }
    }
    componentWillMount () {
        if (localStorage.userid!=''){
            this.setState({hasLogined: true});
            this.setState({userNickName: localStorage.userNickName, userid: localStorage.userid})
        }
    }
    setModalVisible(value){
        this.setState({modalVisible:value})
    }
    handleClick (e) {
        if (e.key == 'register') {
            this.setState({current: 'register'});
            this.setModalVisible(true);
        } else {
            this.setState({current: e.key});
        }
    }
    handleSubmit = (e) => {
        let that = this;
        // 页面开始向API提交参数
        e.preventDefault();
        var myFetchOptions = {
            method: 'GET'
        };
        var formDate = new Object();
        this.props.form.validateFieldsAndScroll((err, values) => {
            formDate = values;
        });
        console.log(this.props.form.getFieldsValue());
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action='+this.state.action+'&username='+formDate.userName+'&password='+formDate.password+'&r_userName='+formDate.r_userName+'&r_password='+formDate.r_password+'&r_confirmPassword='+formDate.r_confirmPassword, myFetchOptions)
        .then(function(response) {
          return response.json()
        }).then(function(json) {
          console.log('parsed json', json);
          that.setState({userNickName: json.NickUserName, userid: json.UserId});
          localStorage.userid = json.UserId;
          localStorage.userNickName = json.NickUserName;
        }).catch(function(ex) {
          console.log('parsing failed', ex)
        });
        if(this.state.action == 'login'){
            this.setState({hasLogined: true});
        }
        message.success('请求成功！');
        this.setModalVisible(false);
    }
    login () {
        this.setModalVisible(true);
    }
    callback (key) {
        if (key==1) {
            this.setState({action:'login'});
        } else {
            this.setState({action:'register'})
        }
    }
    render() {
        let {getFieldDecorator} = this.props.form;
        const userShow = this.state.hasLogined ?
        <Link to={'/usercenter'} className='denglu'>个人中心</Link>
        :
        <span className='denglu' onClick={this.login.bind(this)}>登陆 | 注册</span>

        return (
            <div className='mobileHeader'>
                <header>
                    <i className='iconfont icon-news'></i>
                    <span className='name'>ReactNews</span>
                    {userShow}
                </header>
                <Modal title='用户中心' wrapClassName='vertical-center-modal' visible={this.state.modalVisible} onCancel={()=>this.setModalVisible(false)} onOk={()=>this.setModalVisible(false)} okText='关闭'>
                    <Tabs type='card' onChange={this.callback.bind(this)}>
                        <TabPane tab='登陆' key='1'>
                            <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
                                <FormItem label='账户'>
                                    <Input placeholder='请输入账号' {...getFieldDecorator('userName')} />                                                
                                </FormItem>
                                <FormItem label='密码'>
                                    <Input type='password' placeholder='请输入密码' {...getFieldDecorator('password')} />                                                
                                </FormItem>
                                <Button typee='primary' htmlType='submit'>登陆</Button>
                            </Form>
                        </TabPane>
                        <TabPane tab='注册' key='2'>
                            <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
                                <FormItem label='账户'>
                                    <Input placeholder='请输入账号' {...getFieldDecorator('r_userName')} />                                                
                                </FormItem>
                                <FormItem label='密码'>
                                    <Input type='password' placeholder='请输入密码' {...getFieldDecorator('r_password')} />                                                
                                </FormItem>
                                <FormItem label='确认密码'>
                                    <Input type='password' placeholder='请确认账号' {...getFieldDecorator('r_confirmPassword')} />                                                
                                </FormItem>
                                <Button typee='primary' htmlType='submit'>注册</Button>
                            </Form>
                        </TabPane>   
                    </Tabs>
                </Modal>
            </div>
        )
    }
}

export default MobileHeader = Form.create({})(MobileHeader);