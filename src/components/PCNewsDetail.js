import React from 'react';
import { Row, Col, BackTop } from 'antd';

import PCHeader from '../components/PCHeader'
import PCFooter from '../components/PCFooter'
import PCNewsImageBlock from './PCNewsImageBlock'
import CommonComments from './CommonComments'
class PCNewsDetail extends React.Component {
    constructor () {
        super();
        this.state = {
            newsItem: ''
        }
    }

    componentDidMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnewsitem&uniquekey='+this.props.match.params.uniquekey, myFetchOptions)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            console.log('parsed json', json);
            that.setState({
                newsItem: json
            });
            document.title = this.state.newsItem.title + ' - React News | React 驱动的新闻平台';
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }

    createMarkup () {
        return { __html: this.state.newsItem.pagecontent };
    }

    render () {
        return (
            <div>
                <PCHeader />
                <Row>
                    <Col span={2}></Col>
                    <Col span={14} className='container'>
                        <div className='articleContainer' dangerouslySetInnerHTML={this.createMarkup()}></div>
                        <CommonComments uniquekey={this.props.match.params.uniquekey} />
                    </Col>
                    <Col span={6}>
                        <PCNewsImageBlock count={40} type='top' width='100%' cartTitle='相关新闻' imageWidth='150px' />
                    </Col>
                    <Col span={2}></Col>
                </Row>
                <PCFooter />
                <BackTop />
            </div>
        )
    }
}

export default PCNewsDetail;
