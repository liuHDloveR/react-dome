import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import { Row, Col, Modal, Menu, Icon, Tabs, message, Form, Input, Button, Checkbox, Card, notification, Upload } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const  FormItem = Form.Item;
const TabPane = Tabs.TabPane;

// import PCHeader from '../components/PCHeader'
// import PCFooter from '../components/PCFooter'

class PCUsercenter extends React.Component {
    constructor () {
        super();
        this.state = {
            previewImage : '',
            previewVisible: false
        }
    }

    render () {
        const props = {
            action: 'http://newsapi.gugujiankong.com/handler.ashx',
            headers: {
                'Access-Control-Allow-Origin' : '*'
            },
            listType: 'picture-card',
            defaultFileList: [
                {
                    uid: -1,
                    name: 'xxx.png',
                    state: 'done',
                    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
                    thumbUrl: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png'
                }
            ],
            onChange(info) {
                if (info.file.status !== 'uploading') {
                    console.log(info.file, info.fileList);
                }
                if (info.file.status === 'done') {
                    message.success(`${info.file.name} file uploaded successfully`);
                    this.setState({previewImage: info.file.name});

                } else if (info.file.status === 'error') {
                    message.error(`${info.file.name} file upload failed.`);
                }
            },
        };

        return (
            <div>
                {/*<PCHeader />*/}
                <Tabs>
                    <TabPane tab="我的收藏列表" key="1">

                    </TabPane>
                    <TabPane tab="我的评论列表" key="2">

                    </TabPane>
                    <TabPane tab="头像设置" key="3">
                        <div className="clearfix">
                            <Upload {...props}>
                                <Button>
                                    <Icon type="upload" /> 上传照片
                                </Button>
                            </Upload>
                            <Modal visible = {this.state.previewVisible} footer = {null} onCancel = {this.handleCancel}>
                                <img alt="预览" src={this.state.previewImage} />
                            </Modal>
                        </div>
                    </TabPane>
                </Tabs>
                {/*<PCFooter />*/}
            </div>
        )
    }
}

export default PCUsercenter;
