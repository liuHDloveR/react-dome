import React from 'react';
import { Row, Col, BackTop } from 'antd';

import MobileHeader from '../components/MobileHeader'
import MobileFooter from '../components/MobileFooter'
import CommonComments from './CommonComments'
class MobileNewsDetail extends React.Component {
    constructor () {
        super();
        this.state = {
            newsItem: ''
        }
    }

    componentDidMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnewsitem&uniquekey='+this.props.match.params.uniquekey, myFetchOptions)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            console.log('parsed json', json);
            that.setState({
                newsItem: json
            });
            document.title = this.state.newsItem.title + ' - React News | React 驱动的新闻平台';
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }

    createMarkup () {
        return { __html: this.state.newsItem.pagecontent };
    }

    render () {
        return (
            <div className="mobileDetialsContainer">
                <MobileHeader />
                <div className="ucmobileList">
                    <Row>
                        <Col span={24} className='container'>
                            <div className='articleContainer' dangerouslySetInnerHTML={this.createMarkup()}></div>
                            <CommonComments uniquekey={this.props.match.params.uniquekey} />
                        </Col>
                    </Row>
                    <MobileFooter />
                    <BackTop />
                </div>
            </div>
        )
    }
}

export default MobileNewsDetail;
