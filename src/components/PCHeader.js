import React from 'react';
import { Row, Col, Menu, Icon, message, Form, Input, Button, Checkbox, Modal, Tabs } from 'antd';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const TabPane = Tabs.TabPane;
const FormItem = Form.Item;

class PCHeader extends React.Component {
    constructor () {
        super()
        this.state = {
            current: 'top',
            modalVisible: false,
            action: 'login',
            hasLogined: false,
            userNickName: '',
            userid: 0
        }
    }
    componentWillMount () {
        if (localStorage.userid!=''){
            this.setState({hasLogined: true});
            this.setState({userNickName: localStorage.userNickName, userid: localStorage.userid})
        }
    }
    setModalVisible(value){
        this.setState({modalVisible:value})
    }
    handleClick (e) {
        if (e.key == 'register') {
            this.setState({current: 'register'});
            this.setModalVisible(true);
        } else {
            this.setState({current: e.key});
        }
    }
    handleSubmit = (e) => {
        let that = this;
        // 页面开始向API提交参数
        e.preventDefault();
        var myFetchOptions = {
            method: 'GET'
        };
        var formDate = '';
        this.props.form.validateFields((err, values) => {
            formDate = values;
        });
        console.log(formDate);
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action='+this.state.action+'&username='+formDate.userName+'&password='+formDate.password+'&r_userName='+formDate.r_userName+'&r_password='+formDate.r_password+'&r_confirmPassword='+formDate.r_confirmPassword, myFetchOptions)
        .then(function(response) {
          return response.json()
        }).then(function(json) {
          console.log('parsed json', json);
          that.setState({userNickName: json.NickUserName, userid: json.UserId});
          localStorage.userid = json.UserId;
          localStorage.userNickName = json.NickUserName;
        }).catch(function(ex) {
          console.log('parsing failed', ex)
        });
        if(this.state.action == 'login'){
            this.setState({hasLogined: true});
        }
        message.success('请求成功！');
        this.setModalVisible(false);
    }
    callback (key) {
        if (key==1) {
            this.setState({action:'login'});
        } else {
            this.setState({action:'register'})
        }
    }
    logout() {
        localStorage.userid = '';
        localStorage.userNickName = '';
        this.setState({hasLogined: false});
    }
    render () {
        let {getFieldDecorator} = this.props.form;
        const userShow = this.state.hasLogined
        ?
        <Menu.Item key='logout' className='register'>
            <Button type='primary' htmlType='button'>{this.state.userNickName}</Button>
            &nbsp; &nbsp;
            <Link target='_blank' to={'/usercenter'}>
                <Button type='dashed' htmlType='button'>个人中心</Button>
            </Link>
            &nbsp; &nbsp;
            <Button type='ghost' htmlType='button' onClick={this.logout.bind(this)}>退出</Button>
        </Menu.Item>
        :
        <Menu.Item key='register' className='register'>
            <Icon type='appstore' />登陆 | 注册
        </Menu.Item>;
        
        return (
            <div className='pcheader'>
                <header>                
                    <Row>
                        <Col span={4}><i className='iconfont icon-news'></i><span className='name'>ReactNews</span></Col>
                        <Col span={18}>
                            <Menu mode="horizontal" selectedKeys={[this.state.current]} onClick={this.handleClick.bind(this)}>
                                <Menu.Item key="top"><Icon type="appstore" />头条</Menu.Item>
                                <Menu.Item key="shehui"><Icon type="appstore" />社会</Menu.Item>
                                <Menu.Item key="guonei"><Icon type="appstore" />国内</Menu.Item>
                                <Menu.Item key="guoji"><Icon type="appstore" />国际</Menu.Item>
                                <Menu.Item key="yule"><Icon type="appstore" />娱乐</Menu.Item>
                                <Menu.Item key="tiyu"><Icon type="appstore" />体育</Menu.Item>
                                <Menu.Item key="keji"><Icon type="appstore" />科技</Menu.Item>
                                {userShow}
                            </Menu>

                            <Modal title='用户中心' wrapClassName='vertical-center-modal' visible={this.state.modalVisible} onCancel={()=>this.setModalVisible(false)} onOk={()=>this.setModalVisible(false)} okText='关闭'>
                                <Tabs type='card' onChange={this.callback.bind(this)}>
                                    <TabPane tab='登陆' key='1'>
                                        <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
                                            <FormItem label='账户'>
                                                {getFieldDecorator('userName')(<Input placeholder='请输入账号' /> )}
                                            </FormItem>
                                            <FormItem label='密码'>
                                                {getFieldDecorator('password')(<Input type='password' placeholder='请输入密码' /> )}
                                            </FormItem>
                                            <Button typee='primary' htmlType='submit'>登陆</Button>
                                        </Form>
                                    </TabPane>
                                    <TabPane tab='注册' key='2'>
                                        <Form horizontal onSubmit={this.handleSubmit.bind(this)}>
                                            <FormItem label='账户'>
                                                {getFieldDecorator('r_userName')(<Input placeholder='请输入账号' /> )}
                                            </FormItem>
                                            <FormItem label='密码'>
                                                {getFieldDecorator('r_password')(<Input type='password' placeholder='请输入密码' /> )}
                                            </FormItem>
                                            <FormItem label='确认密码'>
                                                {getFieldDecorator('r_confirmPassword')(<Input type='password' placeholder='请确认密码' /> )}
                                            </FormItem>
                                            <Button typee='primary' htmlType='submit'>注册</Button>
                                        </Form>
                                    </TabPane>
                                </Tabs>
                            </Modal>

                        </Col>
                        <Col span={2}></Col>
                    </Row> 
                </header>
            </div>
        )
    }
}


export default PCHeader = Form.create({})(PCHeader);