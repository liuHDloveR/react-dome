import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import { Row, Col, Modal, Menu, Icon, Tabs, message, Form, Input, Button, Checkbox, Card, notification, Upload } from 'antd';
const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;
const  FormItem = Form.Item;
const TabPane = Tabs.TabPane;

class MobileUserCenter extends React.Component {
    render () {
        return (
            <div>
                <Tabs>
                    <TabPane tab="我的收藏列表" key="1">

                    </TabPane>
                    <TabPane tab="我的评论列表" key="2">

                    </TabPane>
                    <TabPane tab="头像设置" key="3">

                    </TabPane>
                </Tabs>
            </div>
        )
    }
}

export default MobileUserCenter;
