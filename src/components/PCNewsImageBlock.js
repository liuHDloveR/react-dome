import React from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import { Card } from 'antd';

class PCNewsImageBlock extends React.Component {
    constructor () {
        super();
        this.state = {
            news: ''
        }
    }

    componentWillMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type='+this.props.type+'&count='+this.props.count, myFetchOptions)
        .then(function(response) {
          return response.json()
        }).then(function(json) {
        //   console.log('parsed json', json);
        that.setState({
            news: json
        })
        }).catch(function(ex) {
          console.log('parsing failed', ex)
        });
    }

    render () {
        const styleImage = {
            display: 'block',
            width: this.props.imageWidth,
            height: '90px'
        }
        const styleH3 = {
            width: this.props.imageWidth,
            overflow: 'hidden'
        }

        const {news} = this.state;
        // console.log(news);
        const newsList = news.length
        ?
        news.map((newsItem, index)=>(
            <div key={index} className='imageblock'>
                 <Link to={'/details/' + newsItem.uniquekey} target='_blank'>
                    <div className='custom-image'>
                        <img alt='' style={styleImage} src={newsItem.thumbnail_pic_s} />
                    </div>
                    <div className='custom-card'>
                        <h3 style={styleH3}>{newsItem.title}</h3>
                        <p>{newsItem.author_name}</p>
                    </div>
                 </Link>
            </div>
        ))
        :
        '没有加载到任何新闻';

        return (
            <div className='topNewsList'>
                <Card title={this.props.cartTitle} bordered='true' style={{width: this.props.width}}>
                    {newsList}
                </Card>
            </div>
        )
    }
}

export default PCNewsImageBlock;
