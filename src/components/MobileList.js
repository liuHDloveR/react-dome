import React from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import {Row, Col} from 'antd';

class MobileList extends React.Component {
    constructor () {
        super();
        this.state = {
            news: ''
        }
    }

    componentWillMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type='+this.props.type+'&count='+this.props.count, myFetchOptions)
        .then(function(response) {
          return response.json()
        }).then(function(json) {
        //   console.log('parsed json', json);
        that.setState({
            news: json
        })
        }).catch(function(ex) {
          console.log('parsing failed', ex)
        });
    }

    render () {
        const {news} = this.state;
        // console.log(news);
        const newsList = news.length
        ?
        news.map((newsItem, index)=>(
                <section key={index} className='m_article list-item special_section clearfix'>
                     <Link to={'/details/' + newsItem.uniquekey}>
                        <div className='m_article_img'>
                            <img src={newsItem.thumbnail_pic_s} alt={newsItem.title} />
                        </div>
                        <div className='m_article_info'>
                            <div className='m_article_title'>
                                <span>{newsItem.title}</span>
                            </div>
                            <div className='m_article_desc clearfix'>
                                <div className='m_article_desc_l'>
                                    <span className='m_article_channel'>{newsItem.realtype}</span>
                                    <span className='m_article_time'>{newsItem.date}</span>
                                </div>
                            </div>
                        </div>
                     </Link>
                </section>
        ))
        :
        '没有加载到任何新闻';

        return (
            <div>
                <Row>
                    <Col span={24}>
                        {newsList}
                    </Col>
                </Row>
            </div>
        )
    }
}

export default MobileList;
