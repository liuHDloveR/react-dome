import React from 'react';

class MobileFooter extends React.Component {
    render() {
        return (
            <footer>
                2016 ReactNews. &copy; All Rights Reserved.
            </footer>
        )
    }
}

export default MobileFooter;
