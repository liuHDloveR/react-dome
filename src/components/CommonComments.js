import React from 'react';
import { Row, Col, Card, Form, Input, Button, notification } from 'antd';
const FormItem = Form.Item;

class CommonComments extends React.Component {
    constructor () {
        super();
        this.state = {
            comments: ''
        }
    }

    componentDidMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getcomments&uniquekey='+this.props.uniquekey, myFetchOptions)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            console.log('parsed json', json);
            that.setState({comments: json});
        }).catch(function(ex) {
            console.log('parsing failed', ex)
        });
    }

    handleSubmit = (e) => {
        e.preventDefault();
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };

        var formDate = '';
        this.props.form.validateFields((err, values) => {
            formDate = values;
        });
        // console.log(formDate.reark);

        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=comment&userid='+ localStorage.userid +'&uniquekey=' + this.props.uniquekey + '&commnet=' + formDate.reark, myFetchOptions)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            // console.log('parsed json', json);
            that.componentDidMount();
        }).catch(function(ex) {
            // console.log('parsing failed', ex)
        })
    };

    addUserCollection () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };

        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=uc&userid='+ localStorage.userid +'&uniquekey=' + this.props.uniquekey, myFetchOptions)
            .then(function(response) {
                return response.json()
            }).then(function(json) {
            console.log('parsed json', json);
            //收藏成功,提醒
            notification.open({
                message: 'ReactNews提醒',
                description: '此文章收藏成功',
            });

        }).catch(function(ex) {
            console.log('parsing failed', ex);
        })
    }

    render () {
        let {getFieldDecorator} = this.props.form;
        const {comments} = this.state;
        const commentList = comments.length ?
            comments.map((comment, index)=>{
                <Card key={index} title={comment.UserName} extra={<a href="#">发布于 {comment.datatime}</a>}>
                    <p>{comment.comments}</p>
                </Card>
            })
            :
            '没有加载到任何评论';

        return (
            <div className="comment">
                <Row>
                    <Col span={24}>
                        {commentList}
                        <form onSubmit={this.handleSubmit.bind(this)}>
                            <FormItem label="您的评论">
                                {getFieldDecorator('reark')(<Input placeholder='请输入账号' /> )}
                                {/*<Input type="textarea" placeholder="随便写" {...getFieldDecorator('reark', {initialValue: ''})} />*/}
                            </FormItem>
                            <Button type='primary' htmlType="submit">提交评论</Button>
                            <Button type='primary' htmlType="button" onClick={this.addUserCollection.bind(this)}>收藏文章</Button>
                        </form>
                    </Col>
                </Row>
            </div>
        )
    }
}

export default CommonComments = Form.create({})(CommonComments);
