import React from 'react';
import { BrowserRouter as Router, Route, Link, HashRouter, Switch } from 'react-router-dom';
import { Card } from 'antd';

class PCNewsBlock extends React.Component {
    constructor () {
        super();
        this.state = {
            news: ''
        }
    }

    componentWillMount () {
        var that = this;
        var myFetchOptions = {
            method: 'GET'
        };
        fetch('http://newsapi.gugujiankong.com/Handler.ashx?action=getnews&type='+this.props.type+'&count='+this.props.count, myFetchOptions)
        .then(function(response) {
          return response.json()
        }).then(function(json) {
        //   console.log('parsed json', json);
        that.setState({
            news: json
        })
        }).catch(function(ex) {
          console.log('parsing failed', ex)
        });
    }

    render () {
        const {news} = this.state;
        // console.log(news);
        const newsList = news.length
        ?
        news.map((newsItem, index)=>(
            <li key={index}>
                 <Link to={'/details/' + newsItem.uniquekey} target='_blank'>
                    {newsItem.title}
                 </Link>
            </li>
        ))
        :
        '没有加载到任何新闻';

        return (
            <div className='topNewsList'>
                <Card>
                    <ul>{newsList}</ul>
                </Card>
            </div>
        )
    }
}

export default PCNewsBlock;
